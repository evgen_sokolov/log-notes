<?php

namespace App;

use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use GeneratesUuid;

    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $keyType = 'uuid';

    protected $fillable = ['name', 'owner_id'];

    protected $casts = [
        'created_at' => 'date:Y-m-d H:i:s',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function logEntries()
    {
        return $this->hasMany(LogEntry::class);
    }

    public function key()
    {
        return $this->hasOne(Key::class);
    }

    public function logs()
    {
        return $this->hasMany(EventLog::class);
    }
}
