<?php


namespace App\Support\Abstracts;


use App\Key;

interface IEncryptor
{
    static function encrypt(Key $key, $data);
}
