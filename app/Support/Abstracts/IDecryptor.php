<?php


namespace App\Support\Abstracts;


use App\Key;

interface IDecryptor
{
    static function decrypt(Key $key, $data):string;
}
