<?php


namespace App\Services\Storage;


use App\Project;
use App\User;

class EmptyLogStorage implements \App\Support\Abstracts\ISecureLogStorage
{

    public function __construct(User $user, Project $project)
    {
        //pass
    }

    function getAllLogs()
    {
        return null;
    }

    function store(string $text)
    {
        return null;
    }

    function getEncrypted($id)
    {
        return null;
    }

    function getDecrypted($id)
    {
        return null;
    }

    function delete($id)
    {
        return null;
    }
}
