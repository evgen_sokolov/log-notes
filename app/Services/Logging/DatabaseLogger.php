<?php
declare(strict_types=1);

namespace App\Services\Logging;


use App\EventLog;
use App\LogEntry;
use App\Note;
use App\Project;
use App\Support\Abstracts\IEventLogger;
use App\User;

class DatabaseLogger implements IEventLogger
{
    /**
     * @param int $eventType
     * @param array $data
     * @param User $user
     * @param Project|null $project
     * @param LogEntry|null $logEntry
     * @param Note|null $note
     */
    public final function log($eventType = 0, $data = [], ?User $user = null, ?Project $project = null, ?LogEntry $logEntry = null, ?Note $note = null): void
    {
        EventLog::create([
            'event_type' => $eventType,
            'user_id' => $user ? $user->id : null,
            'project_uuid' => $project ? $project->uuid : null,
            'log_entry_uuid' => $logEntry ? $logEntry->uuid : null,
            'note_uuid' => $note ? $note->uuid : null,
            'event_data' => $data,
        ]);
    }

}
