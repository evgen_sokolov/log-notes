<?php


namespace App\Services\Encryption;


use App\Key;
use App\Support\Abstracts\IEncryptor;
use phpseclib\Crypt\RSA as Crypt;

class RSAEncryptor implements IEncryptor
{

    final public static function encrypt(Key $key, $data)
    {
        $rsa = new Crypt();
        $rsa->loadKey($key->public_key);
        $rsa->setEncryptionMode(Crypt::ENCRYPTION_PKCS1);

        return bin2hex($rsa->encrypt($data));
    }
}
