<?php

namespace App\Providers;

use App\Services\Logging\DatabaseLogger;
use App\Support\Abstracts\IEventLogger;
use Illuminate\Support\ServiceProvider;

class LoggerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IEventLogger::class, function ($app) {
            return new DatabaseLogger();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
