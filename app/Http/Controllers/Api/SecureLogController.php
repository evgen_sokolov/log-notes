<?php

namespace App\Http\Controllers\Api;

use App\EventLog;
use App\Http\Controllers\Controller;
use App\LogEntry;
use App\Project;
use App\Support\Abstracts\IEventLogger;
use App\Support\Abstracts\ISecureLogStorage;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SecureLogController extends Controller
{
    final public function create(Request $request, ISecureLogStorage $secureLogStorage, IEventLogger $eventLogger)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $newEntity = $secureLogStorage->store($request->getContent());
        $eventLogger->log(
            EventLog::LOG_ENTRY_CREATED,
            json_encode([
                'ip' => $request->ip(),
                'browser' => $userBrowser ? $userBrowser : 'api call',
                'platform' => $userPlatform ? $userPlatform : 'api'
            ]),
            Auth::user(),
            $newEntity->project,
            $newEntity
        );
        return response()->json($newEntity);
    }

    final public function get(ISecureLogStorage $secureLogStorage, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $secureLogList = $secureLogStorage->getAllLogs();
        $eventLogger->log(
            EventLog::LOG_ENTRY_LIST_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user()
        );
        return response()->json($secureLogList);
    }

    final public function getSingle($project, $logEntry, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $logEntry = LogEntry::findOrFail($logEntry);
        $eventLogger->log(
            EventLog::LOG_ENTRY_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user(),
            null,
            $logEntry
        );
        return response()->json($logEntry);
    }
}
