<?php

namespace App\Http\Controllers;

use App\EventLog;
use App\Support\Abstracts\IEventLogger;
use App\Support\Abstracts\IProjectStorage;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param IProjectStorage $projectStorage
     * @param IEventLogger $eventLogger
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(
        IProjectStorage $projectStorage,
        IEventLogger $eventLogger,
        Request $request
    )
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $projectList = Auth::user()->projects;
        $eventLogger->log(
            EventLog::PROJECT_LIST_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user()
        );
        return view('home', compact('projectList'));
    }
}
