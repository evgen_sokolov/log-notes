<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 *  Thin enough to skip any kind of services, providers, DI
 * **/
class ApiKeyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getKey() {
        return response()->json(Auth::user()->api_key);
    }

    public function setKey() {
        $user = Auth::user();
        $key = $user->api_key;
        if(!$key) {
            $key = Str::random();
            $user = $user->update([
                'api_key' => $key
            ]);
        }

        return response()->json($key);
    }
}
