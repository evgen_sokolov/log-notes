<?php

namespace App\Providers;

use App\Project;
use App\Services\Storage\DatabaseSecureLogStorage;
use App\Services\Storage\EmptyLogStorage;
use App\Support\Abstracts\ISecureLogStorage;
use App\User;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class SecureLogStorageProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Inject GuestNewsService by default
        $this->app->bind(ISecureLogStorage::class, function ($app) {
            return new EmptylogStorage(new User(), new Project());
        });

        //When authenticated, replace with respective NewsService based on user's role
        Event::listen(Authenticated::class, function ($event) {
            if ($this->app->request->route('project')) {
                $project = Project::findOrFail($this->app->request->route('project'));
                $this->app->bind(ISecureLogStorage::class, function ($app) use ($event, $project) {
                    return new DatabaseSecureLogStorage($event->user, $project);
                });
            }
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
