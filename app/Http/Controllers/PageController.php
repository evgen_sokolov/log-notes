<?php

namespace App\Http\Controllers;

use App\EventLog;
use App\LogEntry;
use App\Note;
use App\Project;
use App\Providers\ProjectStorageProvider;
use App\Support\Abstracts\IEventLogger;
use App\Support\Abstracts\IProjectStorage;
use App\Support\Abstracts\ISecureLogStorage;
use App\Support\Abstracts\ISecureNoteStorage;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public final function allProjects(
        Request $request,
        IProjectStorage $projectStorage,
        IEventLogger $eventLogger
    )
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $projectList = Auth::user()->projects()->with('logs')->get();
        $userLogs = Auth::user()->logs;
        $eventLogger->log(
            EventLog::PROJECT_LIST_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user()
        );
        return view('pages.projects.allProjects', compact('projectList', 'userLogs'));
    }

    public final function project(Project $project, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $project = $project->load(['logEntries', 'logs']);
        $eventLogger->log(
            EventLog::PROJECT_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user(),
            $project
        );
        return view('pages.projects.singleProject', compact('project'));
    }

    public final function createProject()
    {
        return view('pages.projects.createProject');
    }

    public final function logEntry($project, $logEntry, ISecureLogStorage $logStorage, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $logEntryEntity = $logStorage->getDecrypted($logEntry)->load(['notes', 'logs']);
        $eventLogger->log(
            EventLog::LOG_ENTRY_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user(),
            $logEntryEntity->project,
            $logEntryEntity
        );
        return view('pages.logEntries.singleLogEntry', compact('logEntryEntity'));
    }

    public final function note($project, $logEntry, $note, ISecureNoteStorage $noteStorage, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $noteEntity = $noteStorage->getDecrypted($note)->load('logs');
        $eventLogger->log(
            EventLog::NOTE_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user(),
            $noteEntity->logEntry->project,
            $noteEntity->logEntry,
            $noteEntity
        );
        return view('pages.notes.singleNote', compact('noteEntity'));
    }
}
