<?php


namespace App\Services\Decryptor;


use App\Key;
use App\Support\Abstracts\IDecryptor;
use phpseclib\Crypt\AES as Crypt;

class AESDecryptor implements IDecryptor
{

    final public static function decrypt(Key $key, $data): string
    {
        $data = hex2bin($data);
        $aes = new Crypt();
        $aes->setKey($key->key);
        $aes->setIV('secret');
        return $aes->decrypt($data);
    }
}
