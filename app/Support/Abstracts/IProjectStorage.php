<?php
declare(strict_types = 1);

namespace App\Support\Abstracts;

use App\User;

interface IProjectStorage
{
    function store(string $name, User $user);
}
