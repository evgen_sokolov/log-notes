<?php
declare(strict_types = 1);

namespace App\Support\Abstracts;


use App\LogEntry;
use App\Note;
use App\Project;
use App\User;

interface IEventLogger
{
    function log(
        int $eventType,
        string $data,
        ?User $user = null,
        ?Project $project = null,
        ?LogEntry $logEntry = null,
        ?Note $note = null);
}
