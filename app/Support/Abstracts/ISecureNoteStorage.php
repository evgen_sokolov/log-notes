<?php
declare(strict_types = 1);

namespace App\Support\Abstracts;


use App\LogEntry;
use App\User;

interface ISecureNoteStorage
{
    function __construct(User $user, LogEntry $logEntry);

    function getAllNotes();

    function store(string $text);

    function getEncrypted($id);

    function getDecrypted($id);

    function delete($id);
}
