/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('all-projects', require('./components/projects/allProjects').default);
Vue.component('create-project', require('./components/projects/createProject').default);
Vue.component('edit-project', require('./components/projects/editProject').default);
Vue.component('all-log-entries', require('./components/logEntries/allLogEntries').default);
Vue.component('create-log-entry', require('./components/logEntries/createLogEntry').default);
Vue.component('edit-log-entry', require('./components/logEntries/editLogEntry').default);
Vue.component('all-notes', require('./components/notes/allNotes').default);
Vue.component('create-note', require('./components/notes/createNote').default);
Vue.component('edit-note', require('./components/notes/editNote').default);
Vue.component('single-note', require('./components/notes/singleNote').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#vueComponent',
});
