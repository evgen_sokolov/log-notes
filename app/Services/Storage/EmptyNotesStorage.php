<?php
declare(strict_types=1);

namespace App\Services\Storage;

use App\LogEntry;
use App\Support\Abstracts\ISecureNoteStorage;
use App\User;

class EmptyNotesStorage implements ISecureNoteStorage
{
    public function __construct(User $user, LogEntry $logEntry)
    {
        //pass
    }

    function getAllNotes()
    {
        return null;
    }

    function store(string $text)
    {
        return null;
    }

    function getEncrypted($id)
    {
        return null;
    }

    function getDecrypted($id)
    {
        return null;
    }

    function delete($id)
    {
        return null;
    }
}
