<?php
declare(strict_types = 1);

namespace App\Support\Abstracts;


use App\Project;
use App\User;

interface ISecureLogStorage
{
    function __construct(User $user, Project $project);

    function getAllLogs();

    function store(string $text);

    function getDecrypted($id);

    function delete($id);
}
