<?php
declare(strict_types = 1);

namespace App\Services\Storage;


use App\LogEntry;
use App\Note;
use App\Services\Decryptor\AESDecryptor;
use App\Services\Encryption\AESEncryptor;
use App\Services\Encryption\RSAEncryptor;
use App\Support\Abstracts\ISecureNoteStorage;
use App\User;

class DatabaseSecureNoteStorage implements ISecureNoteStorage
{
    protected $user;
    protected $logEntry;

    public function __construct(User $user, LogEntry $logEntry)
    {
        $this->user = $user;
        $this->logEntry = $logEntry;
    }

    function getAllNotes()
    {
        return Note::query()->where('log_entry_uuid', $this->logEntry->uuid)->get();
    }

    function store(string $text)
    {
        if (!($this->logEntry->project->key)) abort(500, 'RSA key was not provided for a project');
        return Note::create([
            'log_entry_uuid' => $this->logEntry->uuid,
            'text' => AESEncryptor::encrypt($this->logEntry->project->key, $text),
        ]);
    }

    function getEncrypted($id)
    {
        return Note::query()->where('log_entry_uuid', $this->logEntry->uuid)->findOrFail($id);
    }

    function getDecrypted($id)
    {
        $note=$this->getEncrypted($id);
        $note->text=AESDecryptor::decrypt($this->logEntry->project->key, $note->text );
        return $note;
    }

    function delete($id)
    {
        $note = $this->getEncrypted($id);

        return $note->delete();
    }
}
