<?php

namespace App;

use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class EventLog extends Model
{
    use GeneratesUuid;

    // User events
    public const USER_REGISTERED = 10;
    public const USER_LOGIN = 11;
    public const USER_LOGOUT = 12;

    // Project events
    public const PROJECT_CREATED = 20;
    public const PROJECT_VIEWED = 21;
    public const PROJECT_EDITED = 22;
    public const PROJECT_DELETED = 23;
    public const PROJECT_LIST_VIEWED = 24;

    // Encryption key events
    public const KEY_ADDED = 30;

    // LogEntry events
    public const LOG_ENTRY_CREATED = 40;
    public const LOG_ENTRY_VIEWED = 41;
    public const LOG_ENTRY_LIST_VIEWED = 42;

    // Note events
    public const NOTE_CREATED = 50;
    public const NOTE_VIEWED = 51;
    public const NOTE_LIST_VIEWED = 52;

    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $keyType = 'uuid';

    protected $fillable = ['project_uuid', 'log_entry_uuid', 'user_id', 'note_uuid', 'event_type', 'event_data'];

    protected $casts = [
        'created_at' => 'date:Y-m-d H:i:s',
    ];

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function logEntry()
    {
        return $this->belongsTo(LogEntry::class);
    }

    public function note()
    {
        return $this->belongsTo(Note::class);
    }

    public function key()
    {
        return $this->belongsTo(Key::class);
    }
}
