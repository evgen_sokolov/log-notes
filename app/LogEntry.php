<?php

namespace App;

use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class LogEntry extends Model
{
    use GeneratesUuid;

    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $keyType = 'uuid';

    protected $fillable = ['uuid','user_id', 'project_uuid', 'text'];

    protected $casts = [
        'created_at' => 'date:Y-m-d H:i:s',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function logs()
    {
        return $this->hasMany(EventLog::class);
    }
}
