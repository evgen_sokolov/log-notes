<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->register(SecureNoteStorageProvider::class);
        $this->app->register(EventLoggerServiceProvider::class);
        $this->app->register(ProjectStorageProvider::class);
        $this->app->register(SecureKeyStorageProvider::class);
        $this->app->register(SecureLogStorageProvider::class);
    }
}
