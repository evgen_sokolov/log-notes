<?php

namespace App\Http\Controllers;

use App\EventLog;
use App\Project;
use App\Support\Abstracts\IEventLogger;
use App\Support\Abstracts\IProjectStorage;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    final public function store(
        Request $request,
        IProjectStorage $projectStorage,
        IEventLogger $eventLogger
    )
    {
        /** @noinspection PhpParamsInspection */
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $project = $projectStorage->store($request->get('name'), Auth::user());
        $eventLogger->log(
            EventLog::PROJECT_CREATED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user()
        );
        return response()->json($project);
    }
}
