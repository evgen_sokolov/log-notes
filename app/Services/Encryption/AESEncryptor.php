<?php


namespace App\Services\Encryption;


use App\Key;
use App\Support\Abstracts\IEncryptor;
use phpseclib\Crypt\AES as Crypt;

class AESEncryptor implements IEncryptor
{

    final public static function encrypt(Key $key, $data)
    {
        $aes = new Crypt();
        $aes->setKey($key->key);
        $aes->setIV('secret');
        return bin2hex($aes->encrypt($data));
    }
}
