<?php


namespace App\Services\Storage;


use App\Key;
use App\Project;

class DatabaseSecureKeyStorage implements \App\Support\Abstracts\ISecureKeyStorage
{
    final public function store(string $key, Project $project, int $keyType, int $encryptType)
    {
        return Key::create([
            'project_uuid' => $project->uuid,
            'key' => $key,
            'key_type' => $keyType,
            'encrypt_type' => $encryptType,
        ]);
    }
}
