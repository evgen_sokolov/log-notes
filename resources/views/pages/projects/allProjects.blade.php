@extends('layouts.app')

@section('content')
    <div id="vueComponent">
        <all-projects :projectsList="{{$projectList}}" :userLogs='{{$userLogs}}'/>
    </div>
@endsection
