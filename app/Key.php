<?php

namespace App;

use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    use GeneratesUuid;

    public const RSA = 0;
    public const AES = 1;
    public const PRIVATE = 10;
    public const PUBLIC = 11;
    public const SYMMETRIC = 12;

    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $keyType = 'uuid';

    protected $fillable = ['project_uuid', 'encrypt_type', 'key_type', 'key'];

    protected $casts = [
        'created_at' => 'date:Y-m-d H:i:s',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function logs()
    {
        return $this->hasMany(EventLog::class);
    }
}
