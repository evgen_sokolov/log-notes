<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthenticateByApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $key = $request->header('api-key');
        $user = User::query()->where('api_key', $key)->first();
        if (!$user){
            abort(401);
        }

        Auth::login($user);

        return $next($request);
    }
}
