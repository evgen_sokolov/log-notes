<?php

namespace App;

use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use GeneratesUuid;

    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $keyType = 'uuid';

    protected $fillable = ['log_entry_uuid', 'heading', 'text'];

    protected $casts = [
        'created_at' => 'date:Y-m-d H:i:s',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function logEntry()
    {
        return $this->belongsTo(LogEntry::class, 'log_entry_uuid', 'uuid');
    }

    public function logs()
    {
        return $this->hasMany(EventLog::class);
    }
}
