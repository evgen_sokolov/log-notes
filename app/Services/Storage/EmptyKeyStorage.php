<?php


namespace App\Services\Storage;


use App\Project;

class EmptyKeyStorage implements ISecureKeyStorage
{

    function store(string $key, Project $project)
    {
        return null;
    }
}
