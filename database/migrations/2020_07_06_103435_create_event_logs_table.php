<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_logs', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->foreignUuid('project_uuid')->nullable()->references('uuid')->on('projects');
            $table->foreignUuid('log_entry_uuid')->nullable()->references('uuid')->on('log_entries');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignUuid('note_uuid')->nullable()->references('uuid')->on('notes');
            $table->smallInteger('event_type')->nullable();
            $table->text('event_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_logs');
    }
}
