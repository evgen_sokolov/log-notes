<?php


namespace App\Services\Storage;


use App\Project;
use App\User;

class DatabaseProjectStorage implements \App\Support\Abstracts\IProjectStorage
{

    public final function store(string $name, User $user)
    {
        return Project::create([
            'owner_id' => $user->id,
            'name' => $name,
        ]);
    }
}
