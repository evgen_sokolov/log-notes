<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index' );


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/projects', 'PageController@allProjects')->name('projects');
    Route::get('/project/create', 'PageController@createProject')->name('createProject');
    Route::get('/project/{project}', 'PageController@project')->name('project');
    Route::post('/project/{project}/set-public-key', 'EencryptionKeyController@store')->name('setPublicKey');
    Route::post('/new-project', 'ProjectsController@store');
    Route::get('/project/{project}/log-entry/{logEntry}', 'PageController@logEntry')->name('logEntry');
    Route::get('/project/{project}/log-entry/{logEntry}/note/{note}', 'PageController@note')->name('note');
    Route::get('/get-api-key', 'ApiKeyController@getKey')->name('getAPIKey');
    Route::post('/set-api-key', 'ApiKeyController@setKey')->name('setAPIKey');
    Route::post('/project/{project}/set-aes-key', 'EncryptionKeyController@store')->name('setAESKey');
});
