<?php

namespace App\Providers;

use App\Services\Storage\DatabaseProjectStorage;
use App\Support\Abstracts\IProjectStorage;
use Illuminate\Support\ServiceProvider;

class ProjectStorageProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IProjectStorage::class, function ($app) {
            return new DatabaseProjectStorage();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
