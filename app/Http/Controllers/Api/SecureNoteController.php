<?php

namespace App\Http\Controllers\Api;

use App\EventLog;
use App\Http\Controllers\Controller;
use App\Note;
use App\Support\Abstracts\IEventLogger;
use App\Support\Abstracts\ISecureNoteStorage;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SecureNoteController extends Controller
{
    final public function create(Request $request, ISecureNoteStorage $secureNoteStorage, IEventLogger $eventLogger)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $noteEntity = $secureNoteStorage->store($request->getContent());
        $eventLogger->log(
            EventLog::NOTE_CREATED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user(),
            $noteEntity->logEntry->project,
            $noteEntity->logEntry,
            $noteEntity
        );
        return response()->json($noteEntity);
    }

    final public function get(ISecureNoteStorage $secureNoteStorage, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $noteList = $secureNoteStorage->getAllNotes();
        $eventLogger->log(
            EventLog::NOTE_LIST_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user()
        );
        return response()->json($noteList);
    }

    final public function getSingle($project, $logEntry, $note, IEventLogger $eventLogger, Request $request)
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $note = Note::findOrFail($note);
        $eventLogger->log(
            EventLog::NOTE_VIEWED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user()
        );
        return response()->json($note);
    }
}
