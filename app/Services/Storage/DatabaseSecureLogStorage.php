<?php
declare(strict_types = 1);

namespace App\Services\Storage;


use App\LogEntry;
use App\Note;
use App\Project;
use App\Services\Decryptor\AESDecryptor;
use App\Services\Encryption\AESEncryptor;
use App\Services\Encryption\RSAEncryptor;
use App\Support\Abstracts\ISecureLogStorage;
use App\Support\Abstracts\ISecureNoteStorage;
use App\User;

class DatabaseSecureLogStorage implements ISecureLogStorage
{
    protected $user;
    protected $project;

    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    function getAllLogs()
    {
        return LogEntry::query()->where('user_id', $this->user->id)->get();
    }

    function store(string $text)
    {
        if (!($this->project->key)) abort(500, 'AES key was not provided for a project');
        return LogEntry::create([
            'user_id' => $this->user->id,
            'project_uuid' => $this->project->uuid,
            'text' => AESEncryptor::encrypt($this->project->key, $text),
        ]);
    }

    function getEncrypted($id)
    {
        return LogEntry::query()->where('user_id', $this->user->id)->findOrFail($id);
    }
    function getDecrypted($id)
    {

        $logEntry=LogEntry::query()->where('user_id', $this->user->id)->findOrFail($id);
        $logEntry->text=AESDecryptor::decrypt($this->project->key, $logEntry->text );
        return $logEntry;
    }

    function delete($id)
    {
        $note = $this->getEncrypted($id);

        return $note->delete();
    }
}
