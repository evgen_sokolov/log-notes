<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(\App\Http\Middleware\AuthenticateByApiKey::class)->group(function () {
    // Secure Log
    Route::get('project/{project}/secureLogs', 'Api\SecureLogController@get');
    Route::get('project/{project}/secureLogs/{logEntry}', 'Api\SecureLogController@getSingle');
    Route::post('project/{project}/secureLogs/create','Api\SecureLogController@create');

    // Notes
    Route::get('project/{project}/secureLogs/{logEntry}/notes', 'Api\SecureNoteController@get');
    Route::get('project/{project}/secureLogs/{logEntry}/notes/{note}', 'Api\SecureNoteController@getSingle');
    Route::post('project/{project}/secureLogs/{logEntry}/notes/create', 'Api\SecureNoteController@create');
});
