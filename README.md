## About project

Secret storage for logs and records, using encryption. When developing the project, SOLID, DDD principles were used

## Installation

1. Install dependencies
    composer install
    npm install

2. Rename .env.example to .env
    
    cp .env.example .env

3. Open .env file and setup database connection

4. Run database migrations

    php artisan migrate

5. Run project

    php artisan serve

Enjoy!)