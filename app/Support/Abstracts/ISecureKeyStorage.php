<?php
declare(strict_types = 1);

namespace App\Support\Abstracts;

use App\Project;
use phpDocumentor\Reflection\Types\Integer;

interface ISecureKeyStorage
{
    function store(string $key, Project $project, int $keyType, int $encryptType);
}
