<?php

namespace App\Providers;

use App\Services\Storage\DatabaseSecureKeyStorage;
use App\Support\Abstracts\ISecureKeyStorage;
use Illuminate\Support\ServiceProvider;

class SecureKeyStorageProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ISecureKeyStorage::class, function ($app) {
            return new DatabaseSecureKeyStorage();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
