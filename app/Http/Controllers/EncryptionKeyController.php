<?php

namespace App\Http\Controllers;

use App\EventLog;
use App\Key;
use App\Project;
use App\Support\Abstracts\IEventLogger;
use App\Support\Abstracts\ISecureKeyStorage;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class EncryptionKeyController extends Controller
{
    final public function store(
        Project $project,
        ISecureKeyStorage $keyStorage,
        IEventLogger $eventLogger,
        Request $request
    )
    {
        $userBrowser = Browser::browserName();
        $userPlatform = Browser::platformName();
        $key = $project->key;
        if ($key) abort(400, 'Key already exist');
        $key = Str::random(16);

        $eventLogger->log(
            EventLog::KEY_ADDED,
            json_encode(['ip' => $request->ip(), 'browser' => $userBrowser ? $userBrowser : 'api call', 'platform' => $userPlatform ? $userPlatform : 'api']),
            Auth::user(),
            $project
        );
        Log::error($project);
        return $keyStorage->store($key, $project, Key::SYMMETRIC, Key::AES);
    }
}
