<?php

namespace App\Providers;

use App\LogEntry;
use App\Services\Storage\DatabaseSecureNoteStorage;
use App\Services\Storage\EmptyNotesStorage;
use App\Support\Abstracts\ISecureNoteStorage;
use App\User;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class SecureNoteStorageProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Inject GuestNewsService by default
        $this->app->bind(ISecureNoteStorage::class, function ($app) {
            return new EmptyNotesStorage(new User(), new LogEntry());
        });

        //When authenticated, replace with respective NewsService based on user's role
        Event::listen(Authenticated::class, function ($event) {
            if ($this->app->request->route('logEntry')) {
                $logEntry = LogEntry::findOrFail($this->app->request->route('logEntry'));
                $this->app->bind(ISecureNoteStorage::class, function ($app) use ($event, $logEntry) {
                    return new DatabaseSecureNoteStorage($event->user, $logEntry);
                });
            }

        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
