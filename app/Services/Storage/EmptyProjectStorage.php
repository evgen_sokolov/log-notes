<?php


namespace App\Services\Storage;


use App\User;

class EmptyProjectStorage implements IProjectStorage
{

    function store(string $name, User $user)
    {
        return null;
    }
}
